import Vue from 'vue';
import feathers from '../../feathers';
import { highestSrByPlayer } from '../../services/PlayerUtil';

const eventCategory = 'Pugs';

export default {
  namespaced: true,
  state: {
    pug: {},
  },
  mutations: {
  },
  actions: {
    async load({ state }, pugsUrl) {
      const pugz = await feathers.service('pugs').find(
        {
          query: {
            $limit: 1,
            pugs_url: pugsUrl,
          },
        },
      );
      const [pugs] = pugz.data;
      if (pugs) {
        state.pug = pugs;
      } else {
        console.error('No Pugs at this URL');
      }
      feathers.service('pugs').on('patched', (pug) => {
        state.pug = pug;
      });
    },
    async addToQ({ state, rootState }, roles) {
      await feathers.service('pugs').patch(state.pug._id, { addToQ: rootState.auth.user._id, roles });
      Vue.$gtag.event('addToQ', { event_category: eventCategory, event_label: state.pug.pugs_url, value: rootState.auth.user._id });
    },
    async leaveQ({ state, rootState }) {
      await feathers.service('pugs').patch(state.pug._id, { leaveQ: rootState.auth.user._id });
      Vue.$gtag.event('leaveQ', { event_category: eventCategory, event_label: state.pug.pugs_url, value: rootState.auth.user._id });
    },
    async addToBlue({ state }, playerId) {
      await feathers.service('pugs').patch(state.pug._id, { addToBlue: playerId });
      Vue.$gtag.event('addToBlue', { event_category: eventCategory, event_label: state.pug.pugs_url, value: playerId });
    },
    async addToRed({ state }, playerId) {
      await feathers.service('pugs').patch(state.pug._id, { addToRed: playerId });
      Vue.$gtag.event('addToRed', { event_category: eventCategory, event_label: state.pug.pugs_url, value: playerId });
    },
    async backToQ({ state }, playerId) {
      await feathers.service('pugs').patch(state.pug._id, { backToQ: playerId });
      Vue.$gtag.event('backToQ', { event_category: eventCategory, event_label: state.pug.pugs_url, value: playerId });
    },
    async removePlayer({ state }, playerId) {
      await feathers.service('pugs').patch(state.pug._id, { removePlayer: playerId });
      Vue.$gtag.event('removePlayer', { event_category: eventCategory, event_label: state.pug.pugs_url, value: playerId });
    },
    async clearTeams({ state }) {
      await feathers.service('pugs').patch(state.pug._id, { clearTeams: true });
      Vue.$gtag.event('clearTeams', { event_category: eventCategory, event_label: state.pug.pugs_url });
    },
    async clearQ({ state }) {
      await feathers.service('pugs').patch(state.pug._id, { clearQ: true });
      Vue.$gtag.event('clearQ', { event_category: eventCategory, event_label: state.pug.pugs_url });
    },
    async autoMatch({ state }) {
      await feathers.service('pugs').patch(state.pug._id, { autoMatch: true });
      Vue.$gtag.event('autoMatch', { event_category: eventCategory, event_label: state.pug.pugs_url });
    },
    async updateTitle({ state }) {
      await feathers.service('pugs').patch(state.pug._id, { updateTitle: state.pug.title });
      Vue.$gtag.event('updateTitle', { event_category: eventCategory, event_label: state.pug.pugs_url });
    },
    async addAdmin({ state }, btag) {
      const data = await feathers.service('pugs').patch(state.pug._id, { addAdmin: btag });
      Vue.$gtag.event('addAdmin', { event_category: eventCategory, event_label: state.pug.pugs_url, value: btag });
      return data;
    },
    async removeAdmin({ state }, adminId) {
      await feathers.service('pugs').patch(state.pug._id, { removeAdmin: adminId });
      Vue.$gtag.event('removeAdmin', { event_category: eventCategory, event_label: state.pug.pugs_url, value: adminId });
    },
  },
  getters: {
    sortedUserQueue: (state) => {
      if (state.pug.user_queue) {
        return state.pug.queueorder.map((playerId) => state.pug.user_queue.find((player) => player._id === playerId)).sort((p1, p2) => new Date(p1.updatedAt).getTime() - new Date(p2.updatedAt).getTime());
      }
      return [];
    },
    sortedRedTeam: (state) => {
      if (state.pug.user_queue) {
        return state.pug.redorder.map((playerId) => state.pug.red_team.find((player) => player._id === playerId)).sort((p1, p2) => new Date(p1.updatedAt).getTime() - new Date(p2.updatedAt).getTime());
      }
      return [];
    },
    sortedBlueTeam: (state) => {
      if (state.pug.user_queue) {
        return state.pug.blueorder.map((playerId) => state.pug.blue_team.find((player) => player._id === playerId)).sort((p1, p2) => new Date(p1.updatedAt).getTime() - new Date(p2.updatedAt).getTime());
      }
      return [];
    },
    isAdmin: (state, getters, rootState) => rootState.auth.user && state.pug.admins && (state.pug._id === rootState.auth.user.pugs || state.pug.admins.some((admin) => admin._id === rootState.auth.user._id)),
    inQ: (state, getters, rootState) => rootState.auth.user && state.pug.user_queue && state.pug.user_queue.filter((player) => player._id === rootState.auth.user._id).length > 0,
    inTeam: (state, getters, rootState) => rootState.auth.user && state.pug.red_team && state.pug.blue_team && (state.pug.red_team.filter((player) => player._id === rootState.auth.user._id).length > 0 || state.pug.blue_team.filter((player) => player._id === rootState.auth.user._id).length > 0),
    canAddToRed: (state) => state.pug.red_team && state.pug.red_team.length < 6,
    canAddToBlue: (state) => state.pug.blue_team && state.pug.blue_team.length < 6,
    avgRedSr: (state) => {
      if (state.pug.red_team && state.pug.red_team.length > 0) {
        return Math.round(state.pug.red_team.map((p) => highestSrByPlayer(p)).reduce((p, c) => parseInt(p, 10) + parseInt(c, 10)) / state.pug.red_team.length);
      }
      return 0;
    },
    avgBlueSr: (state) => {
      if (state.pug.blue_team && state.pug.blue_team.length > 0) {
        return Math.round(state.pug.blue_team.map((p) => highestSrByPlayer(p)).reduce((p, c) => parseInt(p, 10) + parseInt(c, 10)) / state.pug.blue_team.length);
      }
      return 0;
    },
  },
};
