import Vue from 'vue';
import feathers from '../../feathers';

const eventCategory = 'Auth';

export default {
  namespaced: true,
  state: {
    loading: false,
    error: '',
    user: null,
  },
  mutations: {
  },
  actions: {
    async reAuth({ state }) {
      try {
        state.loading = true;
        const user = await feathers.reAuthenticate();
        state.user = user.user;
      } catch (error) {
        state.loading = false;
      }
      state.loading = false;
    },
    async login({ state }, { url, label }) {
      Vue.$gtag.event('loginPug', { event_category: eventCategory, event_label: label });
      state.loading = true;
      window.location.href = url;
    },
    async logout({ state }, path) {
      try {
        await feathers.logout();
        console.log(path);
        Vue.$gtag.event('logout', { event_category: eventCategory, event_label: path });
      } catch (error) {
        console.error(error);
      }
      state.user = null;
    },
  },
};
