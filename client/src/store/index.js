import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import pugs from './modules/pugs';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    pugs,
  },
});
