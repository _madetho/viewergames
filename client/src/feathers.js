import io from 'socket.io-client';
import feathers from '@feathersjs/client';
import socketio from '@feathersjs/socketio-client';

let room = 'none';
if (window.location.href.lastIndexOf('#') > -1) {
  room = window.location.href.substring(window.location.href.lastIndexOf('/') + 1, window.location.href.lastIndexOf('#'));
} else {
  room = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
}

if (room.length <= 4) {
  room = 'none';
}

const baseUrl = process.env.VUE_APP_BASE_URL;
const socket = io(`${baseUrl}?room=${room}`, {
  transports: ['websocket', 'polling'],
  forceNew: true,
});

const client = feathers();
client.configure(socketio(socket));
client.configure(feathers.authentication({
  storageKey: 'auth',
  storage: localStorage,
}));

export default client;
