import Vue from 'vue';
import axios from 'axios';
import VueCompositionApi from '@vue/composition-api';
import hooks from '@u3u/vue-hooks';
import Clipboard from 'v-clipboard';
import VueGtag from 'vue-gtag';

import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;
Vue.config.devtools = false;

Object.defineProperty(Vue.prototype, '$http', { value: axios });

Vue.use(hooks);
Vue.use(VueCompositionApi);
Vue.use(Clipboard);

Vue.use(VueGtag, {
  config: {
    id: 'UA-172840622-1',
  },
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
