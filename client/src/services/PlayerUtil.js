export function highestSrByPlayer(player) {
  if (player.stats.isPrivate && Object.keys(player.stats.rank).length === 0) {
    return this.defaultSr;
  }
  const dmgSr = player.stats.rank.damage ? player.stats.rank.damage.sr : 0;
  const supportSr = player.stats.rank.support ? player.stats.rank.support.sr : 0;
  const tankSr = player.stats.rank.tank ? player.stats.rank.tank.sr : 0;
  let highestSr = 0;
  if (dmgSr >= supportSr && dmgSr >= tankSr) {
    highestSr = dmgSr;
  }

  if (supportSr >= dmgSr && supportSr >= tankSr) {
    highestSr = supportSr;
  }

  if (tankSr >= supportSr && tankSr >= dmgSr) {
    highestSr = tankSr;
  }

  return highestSr;
}

export function hasPrivateProfile(player) {
  return player && player.stats.isPrivate;
}

export function hasNoPlacements(player) {
  return player && !player.stats.placements;
}
