import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import Pugs from '../views/Pugs.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/:pugsId',
    name: 'Pugs',
    component: Pugs,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
