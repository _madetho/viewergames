require('dotenv').config();
const express = require('express');
const history = require('connect-history-api-fallback');
const fs = require('fs');
const app = express();

let server;
if (process.env.SSL === 'true') {
    let options = {
        key: fs.readFileSync(process.env.SSL_KEY),
        cert: fs.readFileSync(process.env.SSL_CERT),
        ca: fs.readFileSync(process.env.SSL_CA)
    }
    server = require('https').Server(options, app);
} else {
    server = require('http').Server(app);
}

const staticFileMiddleware = express.static('public');

app.use(staticFileMiddleware);

app.use(history());

app.use(staticFileMiddleware);

server.listen(process.env.PORT, function () {
  console.log('Example app listening on port 3000!');
});