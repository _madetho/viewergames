const { authenticate } = require('@feathersjs/authentication').hooks;



const createPugs = require('../../hooks/create-pugs');



module.exports = {
  before: {
    all: [],
    find: [],
    get: [ authenticate('jwt') ],
    create: [ createPugs() ],
    update: [  authenticate('jwt') ],
    patch: [  authenticate('jwt') ],
    remove: [ authenticate('jwt') ]
  },

  after: {
    all: [ 
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
