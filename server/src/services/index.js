const users = require('./users/users.service.js');
const pugs = require('./pugs/pugs.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(pugs);
};
