// Initializes the `users` service on path `/users`
const { Pugs } = require('./pugs.class');
const createModel = require('../../models/pugs.model');
const hooks = require('./pugs.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/pugs', new Pugs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('pugs');

  service.hooks(hooks);
};
