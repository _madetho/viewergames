const { authenticate } = require('@feathersjs/authentication').hooks;
const populate = require('feathers-populate-hook');



module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [ authenticate('jwt') ],
    update: [  authenticate('jwt') ],
    patch: [  authenticate('jwt') ],
    remove: [ authenticate('jwt') ]
  },

  after: {
    all: [
      async context => {
        if (context.result.data) {
          context.result.data.forEach((pug) => {
            pug.queueorder = pug.user_queue;
            pug.redorder = pug.red_team;
            pug.blueorder = pug.blue_team;
          });
        } else {
          context.result.queueorder = context.result.user_queue;
          context.result.redorder = context.result.red_team;
          context.result.blueorder = context.result.blue_team;
        }
        return context;
      },
      populate.compatibility(),
      populate({
        user_queue: {
          service: 'users',
          f_key: '_id',
        },
        red_team: {
          service: 'users',
          f_key: '_id',
        },
        blue_team: {
          service: 'users',
          f_key: '_id',
        },
        admins: {
          service: 'users',
          f_key: '_id',
        },
      }),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch:  [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
