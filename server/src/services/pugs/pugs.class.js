const { Service } = require('feathers-mongoose');
const logger = require('../../logger');

exports.Pugs = class Pugs extends Service {
  setup(app) {
    this.app = app;
  }

  async patch(id, data, params) {
    const pug = await this.get(id);

    if(data.addToQ) {
      let stats;
      const userToQueue = await this.app.service('users').get(data.addToQ);
      if (Object.keys(userToQueue.stats).length < 4 || Object.keys(userToQueue.stats.rank).length == 0) {
        stats = {};
        stats.rank = {};
        //DPS
        if(data.roles.selectedRoles.includes(0)) {
          stats.rank.damage = { sr: data.roles.currentSr };
        }
        //Tank
        if(data.roles.selectedRoles.includes(1)) {
          stats.rank.tank = { sr: data.roles.currentSr };
        }
        //Support
        if(data.roles.selectedRoles.includes(2)) {
          stats.rank.support = { sr: data.roles.currentSr };
        }
        await this.app.service('users').patch(data.addToQ, { selectedRoles: data.roles.selectedRoles, stats: stats, updatedAt: new Date() });
      } else {
        await this.app.service('users').patch(data.addToQ, { selectedRoles: data.roles.selectedRoles, updatedAt: new Date() });
      }
      pug.user_queue.push(data.addToQ);
      return super.patch(id, { user_queue: pug.user_queue });
    }

    if(data.leaveQ) {
      pug.user_queue = pug.user_queue.filter(player => player._id != data.leaveQ);
      return super.patch(id, { user_queue: pug.user_queue });
    }

    //======== ONLY ADMIN =========
    if(params.user.pugs == id || (pug.admins && pug.admins.some(admin => admin._id.equals(params.user._id)))) {
      if(data.addToBlue) {
        pug.user_queue = pug.user_queue.filter(player => player._id != data.addToBlue);
        pug.red_team = pug.red_team.filter(player => player._id != data.addToBlue);
        pug.blue_team.push(data.addToBlue);
        await this.app.service('users').patch(data.addToBlue, { updatedAt: new Date() });
        return super.patch(id, {
          blue_team: pug.blue_team,
          red_team: pug.red_team,
          user_queue: pug.user_queue,
        });
      }

      if(data.addToRed) {
        pug.user_queue = pug.user_queue.filter(player => player._id != data.addToRed);
        pug.blue_team = pug.blue_team.filter(player => player._id != data.addToRed);
        pug.red_team.push(data.addToRed);
        await this.app.service('users').patch(data.addToRed, { updatedAt: new Date() });
        return super.patch(id, { 
          red_team: pug.red_team,
          blue_team: pug.blue_team,
          user_queue: pug.user_queue,
        });
      }

      if(data.removePlayer) {
        pug.blue_team = pug.blue_team.filter(player => player._id != data.removePlayer);
        pug.red_team = pug.red_team.filter(player => player._id != data.removePlayer);
        return super.patch(id, { 
          red_team: pug.red_team,
          blue_team: pug.blue_team,
        });
      }

      if(data.backToQ) {
        pug.blue_team = pug.blue_team.filter(player => player._id != data.backToQ);
        pug.red_team = pug.red_team.filter(player => player._id != data.backToQ);
        pug.user_queue.unshift(data.backToQ);
        await this.app.service('users').patch(data.backToQ, { updatedAt: new Date() });
        return super.patch(id, {
          blue_team: pug.blue_team,
          red_team: pug.red_team,
          user_queue: pug.user_queue,
        });
      }

      if(data.clearQ) {
        pug.user_queue = [];
        return super.patch(id, { 
          user_queue: pug.user_queue,
        });
      }

      if(data.clearTeams) {
        pug.blue_team = [];
        pug.red_team = [];
        return super.patch(id, { 
          red_team: pug.red_team,
          blue_team: pug.blue_team,
        });
      }

      if(data.addAdmin) {
        const newAdmin = await this.app.service('users').find({
          query: {
            btag: data.addAdmin,
          }
        });
        if(!newAdmin.data[0]) {
          return { message: 'BattleTag™ not found' };
        }
        pug.admins = pug.admins.filter(admin => !admin._id.equals(newAdmin.data[0]._id));
        pug.admins.push(newAdmin.data[0]._id);
        return super.patch(id, {
          admins: pug.admins,
        });
      }

      if(data.removeAdmin) {
        pug.admins = pug.admins.filter(admin => admin._id != data.removeAdmin);
        return super.patch(id, {
          admins: pug.admins,
        });
      }

      if(data.autoMatch) {
        const usersInQ = await this.app.service('users').find({
          query: {
            _id: {
              $in: pug.user_queue
            }
          }
        });
        const usersInRedTeam = await this.app.service('users').find({
          query: {
            _id: {
              $in: pug.red_team
            }
          }
        });
        const usersInBlueTeam = await this.app.service('users').find({
          query: {
            _id: {
              $in: pug.blue_team
            }
          }
        });
        const srRedTeamList = usersInRedTeam.data.map(player => this.highestSrByPlayer(player));
        const srBlueTeamList = usersInBlueTeam.data.map(player => this.highestSrByPlayer(player));
        const srTeamList = srRedTeamList.concat(srBlueTeamList);
        const srQueueList = usersInQ.data.map(player => this.highestSrByPlayer(player));
        const finalSrList = srTeamList.concat(srQueueList.slice(0, (12 - srTeamList.length)));

        let finalBlueTeam = [];
        let finalRedTeam = [];
        const setSize = Math.round(finalSrList.length/2);
        this.divideInEqualSr(finalSrList, finalBlueTeam, finalRedTeam, setSize);

        pug.blue_team = finalBlueTeam.map(bluePlayer => bluePlayer._id);
        pug.red_team = finalRedTeam.map(redPlayer => redPlayer._id);
        pug.user_queue = usersInQ.data.filter(queuePlayer => !pug.blue_team.includes(queuePlayer._id) && !pug.red_team.includes(queuePlayer._id));
        return super.patch(id, { 
          red_team: pug.red_team,
          blue_team: pug.blue_team,
          user_queue: pug.user_queue,
        });
      }

      if(data.updateTitle) {
        return super.patch(id, { 
          title: data.updateTitle,
        });
      }
    } else {
      logger.warn(`${params.user.btag} has no permission to patch pug: ${id}`);
    }
    return super.patch(id, data);
  }

  divideInEqualSr(srArray, blueSrArray, redSrArray, setSize) {
    srArray.sort(this.compareSr);
    let pos1=0;
    let pos2=0;
    
    let i=srArray.length-1;
    
    let sum1 = 0;
    let sum2 = 0;
    while (pos1 < setSize && pos2 < setSize) {
      if (sum1 < sum2){
        blueSrArray[pos1] = srArray[i];
        pos1++;
        sum1 += parseInt(srArray[i].highestSr);
      }else{
        redSrArray[pos2] = srArray[i];
        pos2++;
        sum2 += parseInt(srArray[i].highestSr);
      }
      i--;
    }
    
    while (i>=0) {
      if (pos1 < setSize)
        blueSrArray[pos1++] = srArray[i];
      else
        redSrArray[pos2++] = srArray[i];
      i--;
    }
  }

  compareSr(a, b) {
    if (parseInt(a.highestSr) < parseInt(b.highestSr)) {
      return -1;
    }
    if (parseInt(a.highestSr) > parseInt(b.highestSr)) {
      return 1;
    }
    return 0;
  }

  highestSrByPlayer(player) {
    if (player.stats.isPrivate && Object.keys(player.stats.rank).length === 0) {
      logger.warn(`Could not get highest SR for player: ${player.btag} (${player._id})`);
      return 0;
    }
    const dmgSr = player.stats.rank.damage ? player.stats.rank.damage.sr : 0;
    const supportSr = player.stats.rank.support ? player.stats.rank.support.sr : 0;
    const tankSr = player.stats.rank.tank ? player.stats.rank.tank.sr : 0;
    let highestSr = 0;
    if (dmgSr >= supportSr && dmgSr >= tankSr) {
      highestSr = dmgSr;
    }

    if (supportSr >= dmgSr && supportSr >= tankSr) {
      highestSr = supportSr;
    }

    if (tankSr >= supportSr && tankSr >= dmgSr) {
      highestSr = tankSr;
    }
    player.highestSr = highestSr;
    return player;
  }
};
