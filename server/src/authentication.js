const { AuthenticationService, JWTStrategy } = require('@feathersjs/authentication');
const { OAuthStrategy, expressOauth } = require('@feathersjs/authentication-oauth');
const ow = require('overwatch-stats-api');

class BattlenetStrategy extends OAuthStrategy {
  async getEntityQuery(profile) {
    return {
      battlnet_id: profile.id
    };
  }

  async getEntityData(profile, existing) {
    let stats = {};
    let selectedRoles = [];
    if(existing && existing.stats) {
      stats = existing.stats;
      selectedRoles = existing.selectedRoles;
    }
    try {
      const newOWStats = await ow.getBasicInfo(profile.battletag.replace('#', '-'), 'pc');
      if(Object.keys(newOWStats.rank).length > 0) {
        stats = newOWStats;
        stats.isPrivate = false;
        stats.placements = true;
      } else {
        stats.placements = false;
      }

      if(selectedRoles.length == 0) {
        if (stats.rank && stats.rank.damage) {
          selectedRoles.push(0);
        }
        if (stats.rank && stats.rank.tank) {
          selectedRoles.push(1);
        }
        if (stats.rank && stats.rank.support) {
          selectedRoles.push(2);
        }
      }
    } catch(err) {
      stats.isPrivate = true;
      stats.placements = false;
    }
    const { 
      id: battlnet_id, 
      battletag: btag,
    } = profile; 

    return { battlnet_id, btag, stats, selectedRoles };
  }


}

module.exports = app => {
  const authentication = new AuthenticationService(app);

  authentication.register('jwt', new JWTStrategy());
  authentication.register('battlenet', new BattlenetStrategy());

  app.use('/authentication', authentication);
  app.configure(expressOauth());
};
