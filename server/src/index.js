/* eslint-disable no-console */
const logger = require('./logger');
const fs = require('fs');
const app = require('./app');
const port = app.get('port');
const ssl = app.get('ssl');
const ssl_key = app.get('ssl_key');
const ssl_cert = app.get('ssl_cert');
const ssl_ca = app.get('ssl_ca');

let server;

if(ssl === 'true') {
  server = require('https').createServer({
    key: fs.readFileSync(ssl_key),
    cert: fs.readFileSync(ssl_cert),
    ca: fs.readFileSync(ssl_ca),
  }, app).listen(port);
  app.setup(server);
} else {
  server = app.listen(port);
}


process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

server.on('listening', () =>
  logger.info(`OW-PUGs started: http://${app.get('host')}:${port}`)
);
