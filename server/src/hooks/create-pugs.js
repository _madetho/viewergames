// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { app, data } = context;
    
    if(!data.btag) {
      throw new Error('No Btag found!');
    }

    const pugs = await app.service('pugs').create({
      pugs_url: data.btag.replace('#', '-'),
    });
    
    context.data.pugs = pugs._id;

    return context;
  };
};
